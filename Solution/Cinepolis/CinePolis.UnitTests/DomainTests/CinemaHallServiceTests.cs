﻿using System.Collections.Generic;
using System.Linq;
using CinePolis.Domain.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using CinePolis.Domain.Entities;
using CinePolis.Domain.Services;

namespace CinePolis.Test.DomainTests
{
    [TestClass]
    public class CinemaHallServiceTests
    {
        [TestMethod]
        public void ShouldGetSeatsForHall()
        {
            // Arrange

            const int testCinemaHallId = 1;

            List<Seat> seats = new List<Seat>
            {
                new Seat { Id = 1, CinermaHallId = testCinemaHallId, RowNumber = 1, SeatNumber = 1 },
                new Seat { Id = 2, CinermaHallId = testCinemaHallId + 1, RowNumber = 2, SeatNumber = 2 }
            };

            Mock<ISeatRepository> mockSeatRepository = new Mock<ISeatRepository>();
            mockSeatRepository
                .Setup(sr => sr.GetAll(It.IsAny<int>()))
                .Returns((int cinemaHallId) => seats
                    .Where(s => s.CinermaHallId == cinemaHallId)
                    .ToList());

            CinemaHallService cinemaHallService = new CinemaHallService(mockSeatRepository.Object);

            // Act

            List<Seat> results = cinemaHallService.GetSeatsForHall(testCinemaHallId);
            Seat firstResult = results.FirstOrDefault();

            // Assert

            Assert.IsNotNull(firstResult);
            Assert.AreEqual(testCinemaHallId, firstResult.CinermaHallId);
        }
    }
}