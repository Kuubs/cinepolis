﻿using System.Collections.Generic;
using System.Linq;
using CinePolis.Domain.Entities;
using CinePolis.Domain.Repositories;
using CinePolis.Domain.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace CinePolis.Test.DomainTests
{
    [TestClass]
    public class MovieServiceTests
    {
        [TestMethod]
        public void ShouldGetMovie()
        {
            // Arrange

            const int testMovieId = 1;

            List<Movie> movies = new List<Movie>
            {
                new Movie { Id = 1 },
                new Movie { Id = 2 }
            };

            Mock<IMovieRepository> mockMovieRepository = new Mock<IMovieRepository>();
            mockMovieRepository
                .Setup(mr => mr.GetById(It.IsAny<int>()))
                .Returns((int movieId) => movies.FirstOrDefault(m => m.Id == movieId));

            MovieService movieService = new MovieService(mockMovieRepository.Object);

            // Act

            Movie movie = movieService.GetMovie(testMovieId);

            // Assert

            Assert.IsNotNull(movie);
            Assert.AreEqual(testMovieId, movie.Id);
        }
    }
}