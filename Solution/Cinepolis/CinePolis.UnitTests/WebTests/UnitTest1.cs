﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.WebPages;
using CinePolis.Domain.Services;
using CinePolis.InfraStructure.PdfPrint;
using CinePolis.Web.Touch.Controllers;
using CinePolis.Web.Touch.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace CinePolis.Test.WebTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Print_Redirect_to_errorpage_if_No_tickets_available()
        {
            //Arrange
            Mock<IpdfPrinter> mockPdfPrinter = new Mock<IpdfPrinter>();
            Mock<IOrderService> mockOrderService = new Mock<IOrderService>();
            Mock<IPriceService> mockPriceService = new Mock<IPriceService>();
            Mock<ICinemaHallService> mockCinemaHallService = new Mock<ICinemaHallService>();
           
            PrintController controller = new PrintController(
                mockPdfPrinter.Object,
                mockOrderService.Object,
                mockPriceService.Object,
                mockCinemaHallService.Object);

            // ACT
            ActionResult result = controller.Print();
            
            // assert
            Assert.IsInstanceOfType(result, typeof(ViewResult));

        }
    }
}
