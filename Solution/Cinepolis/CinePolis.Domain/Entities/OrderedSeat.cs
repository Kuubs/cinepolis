﻿namespace CinePolis.Domain.Entities
{
    public class OrderedSeat : IEntity<int>
    {
        public int Id { get; set; }
        public int SeatId { get; set; }
        public int OrderId { get; set; }
        public int ScheduleId { get; set; }
        public bool IsSelected { get; set; }
        public bool IsReserved { get; set; }
    }
}