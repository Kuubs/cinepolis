﻿namespace CinePolis.Domain.Entities
{
    public class Discount : IEntity<int>
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public decimal Amount { get; set; }
        public bool IsPercentage { get; set; }
    }
}