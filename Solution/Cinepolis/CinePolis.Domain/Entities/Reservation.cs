﻿namespace CinePolis.Domain.Entities
{
    public class Reservation : IEntity<int>
    {
        public int Id { get; set; }
        public int ScheduleId { get; set; }
        public int PriceId { get; set; }
        public int OrderId { get; set; }
    }
}