﻿using System.Collections.Generic;

namespace CinePolis.Domain.Entities
{
    public class CinemaHall : IEntity<int>
    {
        public int Id { get; set; }
        public int Capacity { get; set; }
    }
}