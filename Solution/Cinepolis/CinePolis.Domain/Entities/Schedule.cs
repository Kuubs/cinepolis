﻿using System;
using System.Collections.Generic;

namespace CinePolis.Domain.Entities
{
    public class Schedule : IEntity<int>
    {
        public int Id { get; set; }
        public int CinemaHallId { get; set; }
        public int MovieId { get; set; }
        public DateTimeOffset PlayDate { get; set; }
        public bool IsSneakPreview { get; set; }
    }
}