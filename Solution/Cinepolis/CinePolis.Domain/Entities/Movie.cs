﻿using System.Collections.Generic;

namespace CinePolis.Domain.Entities
{
    public class Movie : IEntity<int>
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Plot { get; set; }
        public int Year { get; set; }
        public string Poster { get; set; }
        public int Duration { get; set; }
        public bool IsPlaying { get; set; }
        public string Director { get; set; }
        public List<string> Actors { get; set; }
        public int MinimumAge { get; set; }
    }
}