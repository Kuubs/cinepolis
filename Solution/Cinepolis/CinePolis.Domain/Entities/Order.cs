﻿using System.Collections.Generic;

namespace CinePolis.Domain.Entities
{
    public class Order : IEntity<int>
    {
        public int Id { get; set; }
        public bool IsPaid { get; set; }
    }
}