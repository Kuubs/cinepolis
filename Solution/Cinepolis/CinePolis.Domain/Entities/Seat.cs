﻿namespace CinePolis.Domain.Entities
{
    public class Seat : IEntity<int>
    {
        public int Id { get; set; }
        public int CinermaHallId { get; set; }
        public int Priority { get; set; }
        public int SeatNumber { get; set; }
        public int RowNumber { get; set; }
    }
}