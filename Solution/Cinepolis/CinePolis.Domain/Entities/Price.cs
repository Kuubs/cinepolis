﻿namespace CinePolis.Domain.Entities
{
    public class Price : IEntity<int>
    {
        public decimal Value { get; set; }
        public Discount Discount { get; set; }
        public int Id { get; set; }
        public bool IsTicket { get; set; }

        public decimal CalculateFinalPrice()
        {
            if (Discount.IsPercentage)
                return (Value / 100M) * Discount.Amount;

            return Value - Discount.Amount;
        }
    }
}