﻿using System;
using System.Collections.Generic;
using System.Linq;
using CinePolis.Domain.Entities;
using CinePolis.Domain.Repositories;

namespace CinePolis.Domain.Services
{
    public interface IScheduleService
    {
        List<Schedule> GetScheduledMovies(DateTimeOffset start, DateTimeOffset end);
        Schedule GetSchedule(int scheduleId);
        List<OrderedSeat> GetOrderedSeats(int scheduleId);
    }

    public class ScheduleService : IScheduleService
    {
        private readonly IScheduleRepository _scheduleRepository;
        private readonly IOrderedSeatRepository _orderedSeatRepository;

        public ScheduleService(IOrderedSeatRepository orderedSeatRepository, IScheduleRepository scheduleRepository)
        {
            _orderedSeatRepository = orderedSeatRepository;
            _scheduleRepository = scheduleRepository;
        }

        public List<Schedule> GetScheduledMovies(DateTimeOffset start, DateTimeOffset end)
        {
            return _scheduleRepository.GetByDateRange(start, end);
        }

        public Schedule GetSchedule(int scheduleId)
        {
            return _scheduleRepository.GetById(scheduleId);
        }

        public List<OrderedSeat> GetOrderedSeats(int scheduleId)
        {
            return _orderedSeatRepository.GetByScheduleId(scheduleId);
        }
    }
}