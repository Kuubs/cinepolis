﻿using System.Collections.Generic;
using CinePolis.Domain.Entities;
using CinePolis.Domain.Repositories;

namespace CinePolis.Domain.Services
{
    public interface IPriceService
    {
        List<Price> GetAvailablePrices();
    }

    public class PriceService : IPriceService
    {
        private readonly IPriceRepository _priceRepository;

        public PriceService(IPriceRepository priceRepository)
        {
            _priceRepository = priceRepository;
        }

        public List<Price> GetAvailablePrices()
        {
            return _priceRepository.GetAll();
        }
    }
}