﻿using System.Collections.Generic;
using CinePolis.Domain.Entities;
using CinePolis.Domain.Repositories;

namespace CinePolis.Domain.Services
{
    public interface ICinemaHallService
    {
        List<Seat> GetSeatsForHall(int cinemaHallId);
    }

    public class CinemaHallService : ICinemaHallService
    {
        private readonly ISeatRepository _seatRepository;

        public CinemaHallService(ISeatRepository seatRepository)
        {
            _seatRepository = seatRepository;
        }

        public List<Seat> GetSeatsForHall(int cinemaHallId)
        {
            return _seatRepository.GetAll(cinemaHallId);
        }
    }
}