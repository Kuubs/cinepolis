﻿using System;
using System.Collections.Generic;
using System.Linq;
using CinePolis.Domain.Entities;
using CinePolis.Domain.Repositories;

namespace CinePolis.Domain.Services
{
    public interface IOrderService
    {
        int BeginOrder();
        OrderedSeat SaveSeatSelection(int scheduleId, int seatId, int orderId);
        void RemoveSeatSelection(int scheduleId, int seatId, int orderId);
        void RemoveLastSeatSelections(int count, int scheduleId, int orderId);
        void ProcessOrder(int orderId, int scheduleId, Dictionary<int, int> tickets);
        void CancelOrder(int orderId);
        List<Reservation> GetReservationsForOrder(int orderId);
    }

    public class OrderService : IOrderService
    {
        private readonly IOrderedSeatRepository _orderedSeatRepository;
        private readonly IOrderRepository _orderRepository;
        private readonly IReservationRepository _reservationRepository;

        public OrderService(IOrderedSeatRepository orderedSeatRepository, IOrderRepository orderRepository, IReservationRepository reservationRepository)
        {
            _orderedSeatRepository = orderedSeatRepository;
            _orderRepository = orderRepository;
            _reservationRepository = reservationRepository;
        }

        public int BeginOrder()
        {
            Order order = new Order();
            return _orderRepository.Add(order);
        }

        public OrderedSeat SaveSeatSelection(int scheduleId, int seatId, int orderId)
        {
            OrderedSeat orderedSeat = new OrderedSeat();
            orderedSeat.OrderId = orderId;
            orderedSeat.SeatId = seatId;
            orderedSeat.ScheduleId = scheduleId;
            orderedSeat.IsSelected = true;

            OrderedSeat existingOrderedSeat = _orderedSeatRepository.GetByScheduleAndSeatAndOrderId(scheduleId, seatId, orderId);
            if (existingOrderedSeat != null)
                return existingOrderedSeat;

            _orderedSeatRepository.Add(orderedSeat);
            return orderedSeat;
        }

        public void RemoveSeatSelection(int scheduleId, int seatId, int orderId)
        {
            OrderedSeat orderedSeat = _orderedSeatRepository.GetByScheduleAndSeatAndOrderId(scheduleId, seatId, orderId);
            if (orderedSeat != null)
                _orderedSeatRepository.Delete(orderedSeat);
        }

        public void RemoveLastSeatSelections(int count, int scheduleId, int orderId)
        {
            _orderedSeatRepository.DeleteLastForScheduleAndOrder(count, scheduleId, orderId);
        }

        public void ProcessOrder(int orderId, int scheduleId, Dictionary<int, int> tickets)
        {
            Order order = _orderRepository.GetById(orderId);

            if (order != null)
            {
                order.IsPaid = true;

                List<OrderedSeat> orderedSeats = _orderedSeatRepository.GetByScheduleAndOrderId(scheduleId, orderId);

                foreach (OrderedSeat orderedSeat in orderedSeats)
                {
                    orderedSeat.IsSelected = false;
                    orderedSeat.IsReserved = true;
                    _orderedSeatRepository.Update(orderedSeat);
                }

                foreach (KeyValuePair<int, int> ticket in tickets)
                {
                    int priceId = ticket.Key;
                    int amount = ticket.Value;

                    for (int i = 0; i < amount; i++)
                    {
                        Reservation reservation = new Reservation();
                        reservation.OrderId = orderId;
                        reservation.ScheduleId = scheduleId;
                        reservation.PriceId = priceId;
                        _reservationRepository.Add(reservation);
                    }
                }
            }
        }

        public void CancelOrder(int orderId)
        {
            Order order = _orderRepository.GetById(orderId);
            if (order != null)
                _orderRepository.Delete(order);
        }

        public List<Reservation> GetReservationsForOrder(int orderId)
        {
            return _reservationRepository.GetByOrderId(orderId);
        }
    }
}