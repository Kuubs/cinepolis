﻿using CinePolis.Domain.Entities;
using CinePolis.Domain.Repositories;

namespace CinePolis.Domain.Services
{
    public interface IMovieService
    {
        Movie GetMovie(int movieId);
    }

    public class MovieService : IMovieService
    {
        private readonly IMovieRepository _movieRepository;

        public MovieService(IMovieRepository movieRepository)
        {
            _movieRepository = movieRepository;
        }

        public Movie GetMovie(int movieId)
        {
            return _movieRepository.GetById(movieId);
        }
    }
}