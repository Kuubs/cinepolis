﻿using System;
using System.Collections.Generic;
using System.Linq;
using CinePolis.Domain.Concrete;
using Schedule = CinePolis.Domain.Entities.Schedule;

namespace CinePolis.Domain.Repositories
{
    public interface IScheduleRepository
    {
        List<Schedule> GetByDateRange(DateTimeOffset start, DateTimeOffset end);
        Schedule GetById(int scheduleId);
    }

    public class ScheduleRepository : IScheduleRepository
    {
        private readonly CinePolisEntities _context;

        public ScheduleRepository()
        {
            _context = new CinePolisEntities();
        }

        public List<Schedule> GetByDateRange(DateTimeOffset start, DateTimeOffset end)
        {
            var schedules = _context.GetScheduleForPeriod(start.DateTime, end.DateTime);

            List<Schedule> scheduleList = new List<Schedule>();

            if (schedules != null)
            {
                foreach (var schedule in schedules)
                {
                    scheduleList.Add(new Schedule
                    {
                        Id = schedule.ScheduleID,
                        CinemaHallId = schedule.CinemaHallID,
                        MovieId = schedule.MovieID,
                        PlayDate = schedule.PlayDate ?? new DateTimeOffset(),
                        IsSneakPreview = schedule.IsSneakPreview ?? false
                    });
                }
            }

            return scheduleList;
        }

        public Schedule GetById(int scheduleId)
        {
            var result = _context.GetSchedule(scheduleId);
            if (result != null)
            {
                var schedule = result.FirstOrDefault();

                if (schedule != null)
                {
                    return new Schedule
                    {
                        Id = schedule.ScheduleID,
                        CinemaHallId = schedule.CinemaHallID,
                        MovieId = schedule.MovieID,
                        PlayDate = schedule.PlayDate ?? new DateTimeOffset(),
                        IsSneakPreview = schedule.IsSneakPreview ?? false
                    };
                }
            }

            return null;
        }
    }
}