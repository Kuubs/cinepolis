﻿using System;
using System.Collections.Generic;
using System.Linq;
using CinePolis.Domain.Concrete;
using Reservation = CinePolis.Domain.Entities.Reservation;

namespace CinePolis.Domain.Repositories
{
    public interface IReservationRepository
    {
        int Add(Reservation reservation);
        List<Reservation> GetByOrderId(int orderId);
    }

    public class ReservationRepository : IReservationRepository
    {
        private readonly CinePolisEntities _context;

        public ReservationRepository()
        {
            _context = new CinePolisEntities();
        }

        public int Add(Reservation reservation)
        {
            var result = _context.SetReservation(reservation.OrderId, reservation.ScheduleId, (byte)reservation.PriceId);
            if (result == null)
                return 0;
            int id = Convert.ToInt32(result.FirstOrDefault() ?? 0M);
            reservation.Id = id;
            return id;
        }

        public List<Reservation> GetByOrderId(int orderId)
        {
            List<Reservation> results = new List<Reservation>();

            var reservations = _context.GetReservationsForOrder(orderId);

            if (reservations != null)
            {
                foreach (var reservation in reservations)
                {
                    results.Add(new Reservation
                    {
                        Id = reservation.ReservationID,
                        OrderId = reservation.OrderID ?? 0,
                        PriceId = reservation.PriceID ?? 0,
                        ScheduleId = reservation.ScheduleID ?? 0
                    });
                }
            }

            return results;
        }
    }
}