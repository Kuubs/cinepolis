﻿using System;
using System.Collections.Generic;
using System.Linq;
using CinePolis.Domain.Concrete;
using OrderedSeat = CinePolis.Domain.Entities.OrderedSeat;

namespace CinePolis.Domain.Repositories
{
    public interface IOrderedSeatRepository
    {
        int Add(OrderedSeat orderedSeat);
        void Delete(OrderedSeat orderedSeat);
        OrderedSeat GetByScheduleAndSeatAndOrderId(int scheduleId, int seatId, int orderId);
        List<OrderedSeat> GetByScheduleId(int scheduleId);
        List<OrderedSeat> GetByScheduleAndOrderId(int scheduleId, int orderId);
        void Update(OrderedSeat orderedSeat);
        void DeleteLastForScheduleAndOrder(int count, int scheduleId, int orderId);
    }

    public class OrderedSeatRepository : IOrderedSeatRepository
    {
        private readonly CinePolisEntities _context;

        public OrderedSeatRepository()
        {
            _context = new CinePolisEntities();
        }

        public int Add(OrderedSeat orderedSeat)
        {
            var result = _context.SetOrderedSeat(orderedSeat.SeatId, orderedSeat.OrderId, orderedSeat.ScheduleId, orderedSeat.IsSelected, orderedSeat.IsReserved);
            if (result == null)
                return 0;
            int id = Convert.ToInt32(result.FirstOrDefault() ?? 0M);
            orderedSeat.Id = id;
            return id;
        }

        public void Delete(OrderedSeat orderedSeat)
        {
            _context.DeleteOrderedSeat(orderedSeat.Id);
        }

        public void DeleteLastForScheduleAndOrder(int count, int scheduleId, int orderId)
        {
            _context.DeleteLastOrderedSeatsForScheduleAndOrder(count, scheduleId, orderId);
        }

        public OrderedSeat GetByScheduleAndSeatAndOrderId(int scheduleId, int seatId, int orderId)
        {
            var result = _context.GetOrderdSeatForScheduleAndSeatAndOrder(scheduleId, seatId, orderId);
            
            if (result != null)
            {
                var orderedSeat = result.FirstOrDefault();

                if (orderedSeat != null)
                {
                    return new OrderedSeat
                    {
                        Id = orderedSeat.OrderedSeatID,
                        ScheduleId = orderedSeat.ScheduleID,
                        OrderId = orderedSeat.OrderID,
                        SeatId = orderedSeat.SeatID,
                        IsSelected = orderedSeat.IsSelected,
                        IsReserved = orderedSeat.IsReserved
                    };
                }
            }

            return null;
        }

        public List<OrderedSeat> GetByScheduleId(int scheduleId)
        {
            List<OrderedSeat> orderedSeatList = new List<OrderedSeat>();

            var orderedSeats = _context.GetOrderdSeatsForSchedule(scheduleId);

            if (orderedSeats != null)
            {
                foreach (var orderedSeat in orderedSeats)
                {
                    orderedSeatList.Add(new OrderedSeat
                    {
                        Id = orderedSeat.OrderedSeatID,
                        ScheduleId = orderedSeat.ScheduleID,
                        OrderId = orderedSeat.OrderID,
                        SeatId = orderedSeat.SeatID,
                        IsSelected = orderedSeat.IsSelected,
                        IsReserved = orderedSeat.IsReserved
                    });
                }
            }

            return orderedSeatList;
        }

        public List<OrderedSeat> GetByScheduleAndOrderId(int scheduleId, int orderId)
        {
            List<OrderedSeat> orderedSeatList = new List<OrderedSeat>();

            var orderedSeats = _context.GetOrderdSeatsForScheduleAndOrder(scheduleId, orderId);

            if (orderedSeats != null)
            {
                foreach (var orderedSeat in orderedSeats)
                {
                    orderedSeatList.Add(new OrderedSeat
                    {
                        Id = orderedSeat.OrderedSeatID,
                        ScheduleId = orderedSeat.ScheduleID,
                        OrderId = orderedSeat.OrderID,
                        SeatId = orderedSeat.SeatID,
                        IsSelected = orderedSeat.IsSelected,
                        IsReserved = orderedSeat.IsReserved
                    });
                }
            }

            return orderedSeatList;
        }

        public void Update(OrderedSeat orderedSeat)
        {
            _context.UpdateOrderedSeat(orderedSeat.Id, orderedSeat.IsSelected, orderedSeat.IsReserved);
        }
    }
}