﻿using System;
using System.Linq;
using CinePolis.Domain.Concrete;
using Order = CinePolis.Domain.Entities.Order;

namespace CinePolis.Domain.Repositories
{
    public interface IOrderRepository
    {
        int Add(Order order);
        Order GetById(int orderId);
        void Delete(Order order);
    }

    public class OrderRepository : IOrderRepository
    {
        private readonly CinePolisEntities _context;

        public OrderRepository()
        {
            _context = new CinePolisEntities();
        }

        public int Add(Order order)
        {
            var result = _context.SetOrder(order.IsPaid);
            if (result == null)
                return 0;
            int id = Convert.ToInt32(result.FirstOrDefault() ?? 0M);
            order.Id = id;
            return id;
        }

        public Order GetById(int orderId)
        {
            var result = _context.GetOrder(orderId);

            if (result != null)
            {
                var order = result.FirstOrDefault();

                if (order != null)
                {
                    return new Order
                    {
                        Id = order.OrderID,
                        IsPaid = order.IsPaid ?? false
                    };
                }
            }

            return null;
        }

        public void Delete(Order order)
        {
            _context.DeleteOrder(order.Id);
        }
    }
}