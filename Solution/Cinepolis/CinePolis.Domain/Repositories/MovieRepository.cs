﻿using System.Collections.Generic;
using System.Linq;
using CinePolis.Domain.Concrete;
using Movie = CinePolis.Domain.Entities.Movie;

namespace CinePolis.Domain.Repositories
{
    public interface IMovieRepository
    {
        Movie GetById(int id);
    }

    public class MovieRepository : IMovieRepository
    {
        private readonly CinePolisEntities _context;

        public MovieRepository()
        {
            _context = new CinePolisEntities();
        }

        public Movie GetById(int id)
        {
            var result = _context.GetMovie(id);

            if (result != null)
            {
                var movie = result.FirstOrDefault();

                if (movie != null)
                {
                    return new Movie
                    {
                        Id = movie.MovieID,
                        Title = movie.Title ?? string.Empty,
                        Plot = movie.Plot ?? string.Empty,
                        Year = movie.Year ?? 0,
                        Poster = movie.Poster ?? string.Empty,
                        Duration = movie.Duration ?? 0,
                        IsPlaying = movie.IsPlaying ?? false,

                        // TODO: implement the following
                        Director = string.Empty,
                        Actors = new List<string>(),
                        MinimumAge = 0
                    };
                }
            }

            return null;
        }
    }
}