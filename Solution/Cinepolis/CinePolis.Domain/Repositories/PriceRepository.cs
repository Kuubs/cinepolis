﻿using System.Collections.Generic;
using CinePolis.Domain.Concrete;
using CinePolis.Domain.Entities;
using Price = CinePolis.Domain.Entities.Price;
using Discount = CinePolis.Domain.Entities.Discount;

namespace CinePolis.Domain.Repositories
{
    public interface IPriceRepository
    {
        List<Price> GetAll();
    }

    public class PriceRepository : IPriceRepository
    {
        private readonly CinePolisEntities _context;

        public PriceRepository()
        {
            _context = new CinePolisEntities();
        }

        public List<Price> GetAll()
        {
            var prices = _context.GetPrices();

            List<Price> priceList = new List<Price>();

            if (prices != null)
            {
                foreach (var price in prices)
                {
                    priceList.Add(new Price
                    {
                        Id = price.PriceID,
                        Value = price.Price,
                        IsTicket = price.IsTicket,
                        Discount = new Discount
                        {
                            Id = price.DiscountID,
                            Description = price.Description,
                            Amount = price.DiscountAmount ?? 0,
                            IsPercentage = price.IsPercentage ?? false
                        }
                    });
                }
            }

            return priceList;
        }
    }
}