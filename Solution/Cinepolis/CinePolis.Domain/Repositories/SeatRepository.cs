﻿using System.Collections.Generic;
using CinePolis.Domain.Concrete;
using Seat = CinePolis.Domain.Entities.Seat;

namespace CinePolis.Domain.Repositories
{
    public interface ISeatRepository
    {
        List<Seat> GetAll(int cinemaHallId);
    }

    public class SeatRepository : ISeatRepository
    {
        private readonly CinePolisEntities _context;

        public SeatRepository()
        {
            _context = new CinePolisEntities();
        }

        public List<Seat> GetAll(int cinemaHallId)
        {
            List<Seat> seatList = new List<Seat>();
            
            var seats = _context.GetSeats((byte)cinemaHallId);

            if (seats != null)
            {
                foreach (GetSeatsResult seat in seats)
                {
                    seatList.Add(new Seat
                    {
                        Id = seat.SeatID,
                        CinermaHallId = seat.CinemaHallID,
                        Priority = seat.Priority ?? 0,
                        SeatNumber = seat.SeatNumber ?? 0,
                        RowNumber = seat.Row ?? 0
                    });
                }
            }

            return seatList;
        }
    }
}