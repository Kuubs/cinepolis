﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Spire.Pdf;

namespace CinePolis.InfraStructure.PdfPrint
{
    public class PdfPrinter : IpdfPrinter
    {
        public PdfPrinter()
        {
        }

        public void Print(Byte[] pdf)
        {
            var doc = new PdfDocument();
            doc.LoadFromBytes(pdf);

            //  create instance of printdialog
            var pd = new PrintDialog();

            //  Create instance of printdocument
            PrintDocument pdoc = doc.PrintDocument;

            // create instance of printsettings
            PrinterSettings ps = new PrinterSettings();

            pdoc.Print();

        }

        public void Email(Byte[] pdf)
        {
            throw new NotImplementedException();
        }
    }
}
