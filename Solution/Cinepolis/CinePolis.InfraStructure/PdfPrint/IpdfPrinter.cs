﻿using System;

namespace CinePolis.InfraStructure.PdfPrint
{
    public interface IpdfPrinter
    {
        void Print(Byte[] pdf);

        void Email(Byte[] pdf);
    }
}