﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CinePolis.Web.Touch.Models
{
    public class Ticket
    {
        public string Title { get; set; }
        public int CinemaHallId { get; set; }

        public int Row { get; set; }

        public int SeatNumber { get; set; }

        public int ReservationId { get; set; }

        public int OrderId { get; set; }

        public decimal Price { get; set; }

        public DateTime PlayDate { get; set;  }

    }
}