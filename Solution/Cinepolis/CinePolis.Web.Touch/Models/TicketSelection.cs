﻿namespace CinePolis.Web.Touch.Models
{
    public class TicketSelection
    {
        public int PriceId { get; set; }
        public decimal DefaultPrice { get; set; }
        public decimal FinalPrice { get; set; }
        public string DiscountDescription { get; set; }
        public int SelectedAmount { get; set; }
    }
}