﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CinePolis.Web.Touch.App_Data;

namespace CinePolis.Web.Touch.Models.Payment
{
    public class PinViewModel
    {
        public string MoviePoster { get; set; }
        public string MovieTitle { get; set; }
        public DateTimeOffset ScheduledTime { get; set; }
        public List<TicketSelection> Tickets { get; set; }
        public int TicketCount { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "validationError_PinRequired")]
        [RegularExpression("^(\\d{4})$", ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "validationError_PinInvalid")]
        public string PinCode { get; set; }
    }
}