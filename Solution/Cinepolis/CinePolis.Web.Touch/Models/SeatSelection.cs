﻿namespace CinePolis.Web.Touch.Models
{
    public class SeatSelection
    {
        public int SeatId { get; set; }
        public int RowNumber { get; set; }
        public int SeatNumber { get; set; }
        public bool IsSelectedBySelf { get; set; }
        public bool IsSelectedByOther { get; set; }
        public bool IsReserved { get; set; }
    }
}