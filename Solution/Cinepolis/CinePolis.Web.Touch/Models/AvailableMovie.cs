﻿using System.Collections.Generic;

namespace CinePolis.Web.Touch.Models
{
    public class AvailableMovie
    {
        public string Title { get; set; }
        public int MinumumAge { get; set; }
        public string Director { get; set; }
        public string Poster { get; set; }
        public List<string> Actors { get; set; }
        public List<ScheduleSelection> Schedules { get; set; }
    }
}