﻿using System;

namespace CinePolis.Web.Touch.Models
{
    public class ScheduleSelection
    {
        public DateTimeOffset PlayDate { get; set; }
        public int CinemaHallId { get; set; }
        public int ScheduleId { get; set; }
        public bool IsSelected { get; set; }
        public bool IsSneakPreview { get; set; }
    }
}