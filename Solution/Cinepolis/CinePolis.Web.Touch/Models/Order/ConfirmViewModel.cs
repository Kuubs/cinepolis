﻿using System;

namespace CinePolis.Web.Touch.Models.Order
{
    public class ConfirmViewModel
    {
        public string MoviePoster { get; set; }
        public string MovieTitle { get; set; }
        public DateTimeOffset ScheduledTime { get; set; }
        public int TicketCount { get; set; }
    }
}