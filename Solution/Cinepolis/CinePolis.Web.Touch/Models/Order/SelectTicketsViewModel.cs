﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CinePolis.Web.Touch.App_Data;

namespace CinePolis.Web.Touch.Models.Order
{
    public class SelectTicketsViewModel
    {
        public string MoviePoster { get; set; }
        public string MovieTitle { get; set; }
        public DateTimeOffset ScheduledTime { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "validationError_TicketsRequired")]
        public List<TicketSelection> Tickets { get; set; }
    }
}