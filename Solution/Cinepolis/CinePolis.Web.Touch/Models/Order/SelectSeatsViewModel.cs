﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CinePolis.Web.Touch.App_Data;

namespace CinePolis.Web.Touch.Models.Order
{
    public class SelectSeatsViewModel
    {
        public string MoviePoster { get; set; }
        public string MovieTitle { get; set; }
        public DateTimeOffset ScheduledTime { get; set; }
        public int TicketCount { get; set; }
        public List<SeatSelection> Seats { get; set; }
    }
}