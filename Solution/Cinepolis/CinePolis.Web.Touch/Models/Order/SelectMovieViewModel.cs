﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CinePolis.Web.Touch.App_Data;

namespace CinePolis.Web.Touch.Models.Order
{
    public class SelectMovieViewModel
    {
        public DateTimeOffset SelectedDate { get; set; }
        public List<DateTimeOffset> AvailableDays { get; set; }
        public List<AvailableMovie> Movies { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "validationError_ScheduleRequired")]
        [Range(1, int.MaxValue, ErrorMessageResourceType = typeof(Resources), ErrorMessageResourceName = "validationError_ScheduleRequired")]
        public int SelectedScheduleId { get; set; }
    }
}