﻿using System;
using System.Collections.Generic;

namespace CinePolis.Web.Touch.Models
{
    public class OrderData
    {
        public int OrderId { get; set; }
        /// <summary>
        /// Gets or sets the scheduled film for which to order tickets.
        /// </summary>
        public int ScheduleId { get; set; }
        public int CinemaHallId { get; set; }
        /// <summary>
        /// Gets or sets the seats to reserve.
        /// </summary>
        public List<int> SeatIds { get; set; }

        public Dictionary<int, int> Tickets { get; set; }
        
        
        public string MoviePoster { get; set; }
        public string MovieTitle { get; set; }
        public DateTimeOffset ScheduledTime { get; set; }
        public int TicketCount { get; set; }
        public bool IsProcessed { get; set; }

        public OrderData()
        {
            SeatIds = new List<int>();
        }
    }
}