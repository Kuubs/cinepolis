﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CinePolis.Domain.Entities;
using CinePolis.Domain.Services;
using CinePolis.Web.Touch.App_Data;
using CinePolis.Web.Touch.Models;
using CinePolis.Web.Touch.Models.Payment;

namespace CinePolis.Web.Touch.Controllers
{
    public class PaymentController : Controller
    {
        private readonly IPriceService _priceService;

        public PaymentController(IPriceService priceService)
        {
            _priceService = priceService;
        }

        [HttpGet]
        public ActionResult Pin()
        {
            OrderData orderData = Session["Order"] as OrderData;
            if (orderData == null)
                return RedirectToActionPermanent("Index", "Home");

            List<Price> prices = _priceService.GetAvailablePrices();

            PinViewModel model = new PinViewModel
            {
                MoviePoster = orderData.MoviePoster,
                MovieTitle = orderData.MovieTitle,
                ScheduledTime = orderData.ScheduledTime,
                TicketCount = orderData.TicketCount,
                Tickets = orderData.Tickets
                    .Select(kvp =>
                    {
                        Price price = prices.First(p => p.Id == kvp.Key);

                        return new TicketSelection
                        {
                            PriceId = kvp.Key,
                            SelectedAmount = kvp.Value,
                            DefaultPrice = price.Value,
                            FinalPrice = price.CalculateFinalPrice(),
                            DiscountDescription = price.Discount.Description
                        };
                    })
                    .ToList()
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult Pin(PinViewModel model)
        {
            OrderData orderData = Session["Order"] as OrderData;
            if (orderData == null)
                return RedirectToActionPermanent("Index", "Home");

            if (ModelState.IsValid)
            {
                if (model.PinCode.Any(c => c != '0'))
                    return RedirectToActionPermanent("Confirm", "Order");
                ModelState.AddModelError("PinCode", Resources.validationError_PinInvalid);
            }

            List<Price> prices = _priceService.GetAvailablePrices();

            model.MoviePoster = orderData.MoviePoster;
            model.MovieTitle = orderData.MovieTitle;
            model.ScheduledTime = orderData.ScheduledTime;
            model.TicketCount = orderData.TicketCount;
            model.Tickets = orderData.Tickets
                .Select(kvp =>
                {
                    Price price = prices.First(p => p.Id == kvp.Key);

                    return new TicketSelection
                    {
                        PriceId = kvp.Key,
                        SelectedAmount = kvp.Value,
                        DefaultPrice = price.Value,
                        FinalPrice = price.CalculateFinalPrice(),
                        DiscountDescription = price.Discount.Description
                    };
                })
                .ToList();

            return View(model);
        }
    }
}