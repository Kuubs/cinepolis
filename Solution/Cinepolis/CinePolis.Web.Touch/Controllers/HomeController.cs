﻿using System.Net;
using System.Web.Mvc;
using CinePolis.Domain.Services;
using CinePolis.Web.Touch.Models;
using Ninject.Web.Common;

namespace CinePolis.Web.Touch.Controllers
{
    public class HomeController : Controller
    {
        private readonly IOrderService _orderService;
        public HomeController()
        {
        }

        public HomeController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            OrderData orderData = Session["Order"] as OrderData;
            if (orderData != null)
            {
                if (!orderData.IsProcessed)
                    _orderService.CancelOrder(orderData.OrderId);
                Session["Order"] = null;
            }

            return View();
        }

        public ActionResult MovieOverview()
        {


            return View();
        }

        public ActionResult TicketSelection()
        {


            return View();
        }

        public ActionResult Print()
        {


            return View();
        }

    }
}