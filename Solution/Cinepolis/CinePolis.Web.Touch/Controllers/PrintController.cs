﻿using System;
using System.Collections.Generic;
using System.EnterpriseServices;
using System.Linq;
using System.Web.Mvc;
using System.Web.UI;
using CinePolis.Domain.Entities;
using CinePolis.Domain.Services;
using Rotativa;
using CinePolis.InfraStructure.PdfPrint;
using CinePolis.Web.Touch.Models;
using Microsoft.Ajax.Utilities;

namespace CinePolis.Web.Touch.Controllers
{
    public class PrintController : Controller
    {
        private readonly IOrderService _orderService;
        private readonly IPriceService _priceService;
        private readonly ICinemaHallService _cinemaHallService;

        private readonly IpdfPrinter _pdfPrinter;

        public PrintController(
            IpdfPrinter pdfPrinter,
            IOrderService orderService,
            IPriceService priceService,
            ICinemaHallService cinemaHallService)
        {
            _pdfPrinter = pdfPrinter;
            _orderService = orderService;
            _priceService = priceService;
            _cinemaHallService = cinemaHallService;
        }

        // GET: Print
        public ActionResult Index(Ticket ticketToPrint)
        {
            return View(ticketToPrint);
        }

        [HttpPost]
        public ActionResult Print()
        {
            OrderData orderData = Session["Order"] as OrderData;
            if (orderData == null)
                return RedirectToActionPermanent("Index", "Home");

            List<Price> prices = _priceService.GetAvailablePrices();
            List<Reservation> reservations = _orderService.GetReservationsForOrder(orderData.OrderId);
            List<Seat> seats = _cinemaHallService.GetSeatsForHall(orderData.CinemaHallId).Where(s => orderData.SeatIds.Contains(s.Id)).ToList();

            List<Ticket> ticketList = new List<Ticket>();
            
            foreach (int ticketId in orderData.Tickets.Keys)
            {
                Price price = prices.FirstOrDefault(p => p.Id == ticketId && p.IsTicket);
                if (price == null)
                    continue;

                int amount = orderData.Tickets[ticketId];

                for (int i = 0; i < amount; i++)
                {
                    Reservation reservation = reservations.FirstOrDefault(r => r.OrderId == orderData.OrderId && r.PriceId == price.Id && r.ScheduleId == orderData.ScheduleId);
                    if (reservation == null)
                        break;

                    Seat seat = seats.FirstOrDefault();
                    if (seat == null)
                        break;

                    Ticket ticket = new Ticket();
                    ticket.Title = orderData.MovieTitle;
                    ticket.PlayDate = orderData.ScheduledTime.DateTime;
                    ticket.CinemaHallId = orderData.CinemaHallId;
                    ticket.OrderId = orderData.OrderId;

                    ticket.Price = price.CalculateFinalPrice();
                    ticket.ReservationId = reservation.Id;
                    ticket.Row = seat.RowNumber;
                    ticket.SeatNumber = seat.SeatNumber;

                    ticketList.Add(ticket);

                    reservations.Remove(reservation);
                    seats.Remove(seat);
                }
            }

            bool succes = false;
            PrintTicketList(ticketList, out succes);

            if (succes)
            {
                return RedirectToAction("Index", "Home");  
            }
            else
            {
                return View("PrintError");
            }

            
        }


     


        private void PrintTicketList(IList<Ticket> ticketList, out bool success)
        {
            success = false;
            if (ticketList != null)
            {
                foreach (Ticket ticket in ticketList)
                {
                    try
                    {
                        // create new instance of print object and fill with data to print, passed through by calling method

                        var actionAsPdf = new ActionAsPdf("Index", ticket)
                        {
                            FileName =
                                string.Format("{0}{1}.pdf", ticket.OrderId.ToString(),
                                    arg1: ticket.ReservationId.ToString())
                        };
                       
                        var pdfToByteArray = actionAsPdf.BuildPdf(ControllerContext);
                        _pdfPrinter.Print(pdfToByteArray);
                        success = true;
                    }
                    catch (Exception)
                    {

                        success = false;
                    }
                }
            }
            else
            {

                success = false;
            }
        }
    }
}