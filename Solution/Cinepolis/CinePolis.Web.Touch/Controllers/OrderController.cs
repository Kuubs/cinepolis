﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using CinePolis.Domain.Entities;
using CinePolis.Domain.Services;
using CinePolis.Web.Touch.App_Data;
using CinePolis.Web.Touch.Models;
using CinePolis.Web.Touch.Models.Order;

namespace CinePolis.Web.Touch.Controllers
{
    public class OrderController : Controller
    {
        private readonly IOrderService _orderService;
        private readonly IScheduleService _scheduleService;
        private readonly IPriceService _priceService;
        private readonly IMovieService _movieService;
        private readonly ICinemaHallService _cinemaHallService;

        public OrderController(
            IOrderService orderService,
            IScheduleService scheduleService,
            IPriceService priceService,
            IMovieService movieService,
            ICinemaHallService cinemaHallService)
        {
            _orderService = orderService;
            _scheduleService = scheduleService;
            _priceService = priceService;
            _movieService = movieService;
            _cinemaHallService = cinemaHallService;
        }

        [HttpGet]
        public ActionResult SelectMovie(DateTimeOffset? date)
        {
            DateTimeOffset now = DateTimeOffset.Now;
            DateTimeOffset selectedDate = date.HasValue && date.Value >= now
                ? new DateTimeOffset(date.Value.Date, date.Value.Offset)
                : new DateTimeOffset(now.Date, now.Offset);

            DateTimeOffset startDate = selectedDate >= now.Date.AddDays(1)
                ? selectedDate
                : now;
            DateTimeOffset endDate = selectedDate.AddDays(1);

            OrderData orderData = Session["Order"] as OrderData;
            if (orderData == null)
            {
                orderData = new OrderData {OrderId = _orderService.BeginOrder()};
                Session["Order"] = orderData;
            }

            List<Schedule> schedules = _scheduleService.GetScheduledMovies(startDate, endDate);

            List<DateTimeOffset> days = new List<DateTimeOffset>();
            DateTimeOffset nextDate = new DateTimeOffset(now.Date, now.Offset);
            do
            {
                days.Add(nextDate);
                nextDate = nextDate.AddDays(1);
            }
            while (nextDate.DayOfWeek != now.DayOfWeek);

            SelectMovieViewModel model = new SelectMovieViewModel
            {
                SelectedDate = selectedDate,
                AvailableDays = days,
                Movies = schedules.GroupBy(s => s.MovieId, s => s)
                    .Select(kvp =>
                    {
                        Movie movie = _movieService.GetMovie(kvp.Key);

                        return new AvailableMovie
                        {
                            Poster = movie.Poster,
                            Title = movie.Title,
                            Director = movie.Director,
                            Actors = movie.Actors,
                            MinumumAge = movie.MinimumAge,
                            Schedules = kvp.Select(v => new ScheduleSelection
                            {
                                ScheduleId = v.Id,
                                CinemaHallId = v.CinemaHallId,
                                IsSelected = v.Id == orderData.ScheduleId,
                                PlayDate = v.PlayDate,
                                IsSneakPreview = v.IsSneakPreview
                            }).ToList()
                        };
                    }).ToList()
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult SelectMovie(SelectMovieViewModel model)
        {
            OrderData orderData = Session["Order"] as OrderData;
            if (orderData == null)
                return RedirectToActionPermanent("Index", "Home");

            DateTimeOffset now = DateTimeOffset.Now;
            DateTimeOffset selectedDate = model.SelectedDate >= now
                ? new DateTimeOffset(model.SelectedDate.Date, model.SelectedDate.Offset)
                : new DateTimeOffset(now.Date, now.Offset);

            DateTimeOffset startDate = selectedDate >= now.Date.AddDays(1)
                ? selectedDate
                : now;
            DateTimeOffset endDate = selectedDate.AddDays(1);

            List<Schedule> schedules = _scheduleService.GetScheduledMovies(startDate, endDate);

            if (ModelState.IsValid)
            {
                orderData.ScheduleId = model.SelectedScheduleId;
                Schedule schedule = schedules.First(s => s.Id == model.SelectedScheduleId);
                Movie movie = _movieService.GetMovie(schedule.MovieId);
                orderData.MovieTitle = movie.Title;
                orderData.ScheduledTime = schedule.PlayDate;
                orderData.CinemaHallId = schedule.CinemaHallId;
                orderData.MoviePoster = movie.Poster;
                return RedirectToActionPermanent("SelectTickets");
            }

            List<DateTimeOffset> days = new List<DateTimeOffset>();
            DateTimeOffset nextDate = now;

            do
            {
                days.Add(nextDate);
                nextDate = nextDate.AddDays(1);
            }
            while (nextDate.DayOfWeek != now.DayOfWeek);

            model.AvailableDays = days;
            model.Movies = schedules.GroupBy(s => s.MovieId, s => s)
                .Select(kvp =>
                {
                    Movie movie = _movieService.GetMovie(kvp.Key);

                    return new AvailableMovie
                    {
                        Poster = movie.Poster,
                        Title = movie.Title,
                        Director = movie.Director,
                        Actors = movie.Actors,
                        MinumumAge = movie.MinimumAge,
                        Schedules = kvp.Select(v => new ScheduleSelection
                        {
                            ScheduleId = v.Id,
                            CinemaHallId = v.CinemaHallId,
                            IsSelected = v.Id == orderData.ScheduleId,
                            IsSneakPreview = v.IsSneakPreview
                        }).ToList()
                    };
                }).ToList();

            return View(model);
        }

        [HttpGet]
        public ActionResult SelectTickets()
        {
            OrderData orderData = Session["Order"] as OrderData;
            if (orderData == null)
                return RedirectToActionPermanent("Index", "Home");

            List<Price> prices = _priceService.GetAvailablePrices();

            SelectTicketsViewModel model = new SelectTicketsViewModel
            {
                MoviePoster = orderData.MoviePoster,
                MovieTitle = orderData.MovieTitle,
                ScheduledTime = orderData.ScheduledTime,
                Tickets = prices.Select(p =>
                {
                    TicketSelection ticketSelection = new TicketSelection
                    {
                        PriceId = p.Id,
                        DefaultPrice = p.Value,
                        FinalPrice = p.CalculateFinalPrice(),
                        DiscountDescription = p.Discount.Description
                    };

                    if (orderData.Tickets != null)
                    {
                        int selectedAmount;
                        orderData.Tickets.TryGetValue(p.Id, out selectedAmount);
                        ticketSelection.SelectedAmount = selectedAmount;
                    }

                    return ticketSelection;
                })
                .ToList()
            };

            return View(model);
        }

        [HttpPost]
        public ActionResult SelectTickets(SelectTicketsViewModel model)
        {
            OrderData orderData = Session["Order"] as OrderData;
            if (orderData == null)
                return RedirectToActionPermanent("Index", "Home");

            List<Price> prices = _priceService.GetAvailablePrices();

            if (ModelState.IsValid)
            {
                if (model.Tickets.Any(t => t.SelectedAmount > 0))
                {
                    orderData.Tickets = model.Tickets
                        .Where(t => t.SelectedAmount > 0)
                        .ToDictionary(t => t.PriceId, t => t.SelectedAmount);

                    int previousTicketCount = orderData.TicketCount;
                    orderData.TicketCount = orderData.Tickets.Where(kvp => prices.Any(p => p.Id == kvp.Key && p.IsTicket)).Sum(kvp => kvp.Value);

                    if (previousTicketCount > 0 && orderData.TicketCount < previousTicketCount)
                        _orderService.RemoveLastSeatSelections(previousTicketCount - orderData.TicketCount, orderData.ScheduleId, orderData.OrderId);

                    return RedirectToActionPermanent("SelectSeats");
                }

                ModelState.AddModelError("Tickets", Resources.validationError_TicketsRequired);
            }

            model.MoviePoster = orderData.MoviePoster;
            model.MovieTitle = orderData.MovieTitle;
            model.ScheduledTime = orderData.ScheduledTime;
            model.Tickets = prices
                .Select(p =>
                {
                    TicketSelection ticketSelection = new TicketSelection
                    {
                        PriceId = p.Id,
                        DefaultPrice = p.Value,
                        FinalPrice = p.CalculateFinalPrice(),
                        DiscountDescription = p.Discount.Description
                    };

                    if (orderData.Tickets != null)
                    {
                        int selectedAmount;
                        orderData.Tickets.TryGetValue(p.Id, out selectedAmount);
                        ticketSelection.SelectedAmount = selectedAmount;
                    }

                    return ticketSelection;
                })
                .ToList();

            return View(model);
        }

        [HttpGet]
        public ActionResult SelectSeats()
        {
            OrderData orderData = Session["Order"] as OrderData;
            if (orderData == null)
                return RedirectToActionPermanent("Index", "Home");

            List<OrderedSeat> orderedSeats = _scheduleService.GetOrderedSeats(orderData.ScheduleId);
            List<Seat> seats = _cinemaHallService.GetSeatsForHall(orderData.CinemaHallId);

            SelectSeatsViewModel model = new SelectSeatsViewModel
            {
                MoviePoster = orderData.MoviePoster,
                MovieTitle = orderData.MovieTitle,
                ScheduledTime = orderData.ScheduledTime,
                Seats = seats
                    .Select(s =>
                    {
                        OrderedSeat orderedSeat = orderedSeats.FirstOrDefault(os => os.SeatId == s.Id);

                        return new SeatSelection
                        {
                            SeatId = s.Id,
                            RowNumber = s.RowNumber,
                            SeatNumber = s.SeatNumber,
                            IsReserved = orderedSeat != null && orderedSeat.IsReserved,
                            IsSelectedByOther = orderedSeat != null && orderedSeat.IsSelected && orderedSeat.OrderId != orderData.OrderId,
                            IsSelectedBySelf = orderedSeat != null && orderedSeat.OrderId == orderData.OrderId
                        };
                    })
                    .OrderBy(s => s.RowNumber)
                    .ThenBy(s => s.SeatNumber)
                    .ToList()
            };

            model.TicketCount = orderData.TicketCount;

            return View(model);
        }

        [HttpPost]
        public ActionResult SelectSeats(SelectSeatsViewModel model)
        {
            OrderData orderData = Session["Order"] as OrderData;
            if (orderData == null)
                return RedirectToActionPermanent("Index", "Home");

            if (ModelState.IsValid)
            {
                if (orderData.SeatIds.Count == orderData.TicketCount)
                {
                    return RedirectToActionPermanent("SelectPayment");
                }

                ModelState.AddModelError("SelectedSeatIds", Resources.validationError_SeatCountInvalid);
            }

            List<OrderedSeat> orderedSeats = _scheduleService.GetOrderedSeats(orderData.ScheduleId);
            List<Seat> seats = _cinemaHallService.GetSeatsForHall(orderData.CinemaHallId);

            model.MoviePoster = orderData.MoviePoster;
            model.MovieTitle = orderData.MovieTitle;
            model.ScheduledTime = orderData.ScheduledTime;
            model.Seats = seats
                .Select(s =>
                {
                    OrderedSeat orderedSeat = orderedSeats.FirstOrDefault(os => os.SeatId == s.Id);

                    return new SeatSelection
                    {
                        SeatId = s.Id,
                        RowNumber = s.RowNumber,
                        SeatNumber = s.SeatNumber,
                        IsReserved = orderedSeat != null && orderedSeat.IsReserved,
                        IsSelectedByOther =
                            orderedSeat != null && orderedSeat.IsSelected && orderedSeat.OrderId != orderData.OrderId,
                        IsSelectedBySelf = orderedSeat != null && orderedSeat.OrderId == orderData.OrderId
                    };
                })
                .OrderBy(s => s.RowNumber)
                .ThenBy(s => s.SeatNumber)
                .ToList();

            model.TicketCount = orderData.TicketCount;

            return View(model);
        }

        public ActionResult SaveSeatSelection(int seatId)
        {
            OrderData orderData = Session["Order"] as OrderData;
            
            if (orderData != null && !orderData.SeatIds.Contains(seatId) && orderData.SeatIds.Count < orderData.TicketCount)
            {
                OrderedSeat orderedSeat = _orderService.SaveSeatSelection(orderData.ScheduleId, seatId, orderData.OrderId);

                if (orderedSeat.OrderId == orderData.OrderId)
                    orderData.SeatIds.Add(seatId);

                return Json(new
                {
                    IsSelected = orderedSeat.OrderId == orderData.OrderId,
                    IsAlreadySelectedByOther = orderedSeat.IsSelected && orderedSeat.OrderId != orderData.OrderId,
                    IsAlreadyReservedByOther = orderedSeat.IsReserved && orderedSeat.OrderId != orderData.OrderId
                });
            }

            return Json(new
            {
                IsSelected = false,
                IsAlreadySelectedByOther = false,
                IsAlreadyReservedByOther = false
            });
        }

        public ActionResult RemoveSeatSelection(int seatId)
        {
            OrderData orderData = Session["Order"] as OrderData;

            if (orderData != null && orderData.SeatIds.Contains(seatId))
            {
                _orderService.RemoveSeatSelection(orderData.ScheduleId, seatId, orderData.OrderId);
                orderData.SeatIds.RemoveAll(s => s == seatId);
            }

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        [HttpGet]
        public ActionResult SelectPayment()
        {
            OrderData orderData = Session["Order"] as OrderData;
            if (orderData == null)
                return RedirectToActionPermanent("Index", "Home");

            SelectPaymentViewModel model = new SelectPaymentViewModel
            {
                MoviePoster = orderData.MoviePoster,
                MovieTitle = orderData.MovieTitle,
                ScheduledTime = orderData.ScheduledTime,
                TicketCount = orderData.TicketCount
            };

            return View(model);
        }

        [HttpGet]
        public ActionResult Confirm()
        {
            OrderData orderData = Session["Order"] as OrderData;
            if (orderData == null)
                return RedirectToActionPermanent("Index", "Home");

            _orderService.ProcessOrder(orderData.OrderId, orderData.ScheduleId, orderData.Tickets);
            orderData.IsProcessed = true;

            ConfirmViewModel model = new ConfirmViewModel
            {
                MoviePoster = orderData.MoviePoster,
                MovieTitle = orderData.MovieTitle,
                ScheduledTime = orderData.ScheduledTime,
                TicketCount = orderData.TicketCount
            };

            return View(model);
        }
    }
}