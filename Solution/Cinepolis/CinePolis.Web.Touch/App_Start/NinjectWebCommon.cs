using CinePolis.Domain.Repositories;
using CinePolis.Domain.Services;
using CinePolis.InfraStructure.PdfPrint;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(CinePolis.Web.Touch.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(CinePolis.Web.Touch.App_Start.NinjectWebCommon), "Stop")]

namespace CinePolis.Web.Touch.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<IpdfPrinter>().To<PdfPrinter>();

            // Repositories
            kernel.Bind<IMovieRepository>().To<MovieRepository>();
            kernel.Bind<IOrderedSeatRepository>().To<OrderedSeatRepository>();
            kernel.Bind<IOrderRepository>().To<OrderRepository>();
            kernel.Bind<IPriceRepository>().To<PriceRepository>();
            kernel.Bind<IReservationRepository>().To<ReservationRepository>();
            kernel.Bind<IScheduleRepository>().To<ScheduleRepository>();
            kernel.Bind<ISeatRepository>().To<SeatRepository>();

            // Services
            kernel.Bind<ICinemaHallService>().To<CinemaHallService>();
            kernel.Bind<IMovieService>().To<MovieService>();
            kernel.Bind<IOrderService>().To<OrderService>();
            kernel.Bind<IPriceService>().To<PriceService>();
            kernel.Bind<IScheduleService>().To<ScheduleService>();
        }        
    }
}
